const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let clisents3 = [...clients1, ...clients2];

const uniqueClients = (clisents3) => {
  return clisents3.filter((el, id) => clisents3.indexOf(el) === id);
};

console.log(uniqueClients(clisents3));
