// ### Задание 6

// Дан обьект `employee`. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.

// ```javascript
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const obj = { ...employee, age: 90, salary: 2222 };
const newObj = Object.assign({ age: 90, salary: 2222 }, employee);

console.log(obj);
console.log(newObj);
